#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
// #include "CPU.h"



using namespace std;


#define eps 1.0e-7
typedef double TMatrix;
typedef int   TMatrixDimension;

__device__ void GetMaxTidLocalGPU(TMatrixDimension nLocal, TMatrixDimension mLocal, TMatrixDimension &tidMax);
__device__ void GetSizeSubMatrixGPU(TMatrixDimension n, TMatrixDimension m, TMatrixDimension row, TMatrixDimension column, TMatrixDimension &nLocal, TMatrixDimension &mLocal);
__device__ void TransformatioCoordsTidGPU(TMatrixDimension tidLocal, TMatrixDimension &tid, TMatrixDimension mLocal, TMatrixDimension m, TMatrixDimension row0, TMatrixDimension column0);
__device__ void GetMuTidGPU(TMatrixDimension tidLocal, TMatrixDimension mLocal, TMatrixDimension row0, TMatrixDimension column0, TMatrixDimension m, TMatrixDimension &tidLocalMu);
__device__ void GetCurrentElementTidGPU(TMatrixDimension tidLocal, TMatrixDimension mLocal, TMatrixDimension row0, TMatrixDimension column0, TMatrixDimension m, TMatrixDimension &tidLocalCurrentElement);
__device__ void SwapElementsGPU(TMatrix &a, TMatrix &b);
__global__ void ModifyMatrixGPU(TMatrix *a, TMatrixDimension *dev_column0, TMatrixDimension *dev_row0, TMatrixDimension *dev_n, TMatrixDimension *dev_m);
__global__ void GetMuGPU(TMatrix *a, TMatrixDimension *dev_column0, TMatrixDimension *dev_row0, TMatrixDimension *dev_m);
__global__ void GetIndexOfMaxElementGPU(TMatrix *a, TMatrixDimension *column, TMatrixDimension row, TMatrixDimension n, TMatrixDimension m);
__global__ void SwapGPU(TMatrix *a, TMatrixDimension *dev_m, TMatrixDimension *dev_row1, TMatrixDimension *dev_row2);
__device__ TMatrixDimension GetLineIndexGPU(TMatrixDimension row, TMatrixDimension column, TMatrixDimension m);
TMatrix * ReadElemetsOfMatrixCPU(TMatrixDimension n, TMatrixDimension m);
TMatrixDimension GetLineIndexCPU(TMatrixDimension i, TMatrixDimension j, TMatrixDimension m);
TMatrixDimension GetRowByLineIndexCPU(TMatrixDimension lineIndex, TMatrixDimension m);
TMatrixDimension GetRangGPU(TMatrix *dev_a, TMatrixDimension *dev_n, TMatrixDimension *dev_m, TMatrixDimension n, TMatrixDimension m);
void GetBlockThread(TMatrixDimension poles, TMatrixDimension &blocks, TMatrixDimension &threads);
void SwapRowsGPU(TMatrix *dev_a, TMatrixDimension *dev_n, TMatrixDimension *dev_m, TMatrixDimension n, TMatrixDimension m, TMatrixDimension *curColumn, TMatrixDimension curRow);

#define HANDLE_ERROR(call) {											\
    cudaError err = call;												\
    if(err != cudaSuccess) {											\
        fprintf(stderr, "CUDA error in file '%s' in line %i: %s.\n",	\
            __FILE__, __LINE__, cudaGetErrorString(err));				\
        exit(1);														\
    }																	\
} while (0)

/*___________________GPU_GENERAL_FUNCTIONS___________________*/


__global__ void ModifyMatrixGPU(TMatrix *a, 
									TMatrixDimension *dev_column0, TMatrixDimension *dev_row0, 
									TMatrixDimension *dev_n, TMatrixDimension *dev_m)
{
	// ��������� �������� �� ���������� (������������ ��� ���������, ��������� �� ����������)
	TMatrixDimension n = 0;
	TMatrixDimension m = 0;
	TMatrixDimension row0 = 0;
	TMatrixDimension column0 = 0;
	// ������� ����������
	TMatrixDimension nLocal = 0;
	TMatrixDimension mLocal = 0;
	// ����� ����������
	TMatrixDimension tidLocal = 0;
	// ���������� ���������, ����������� ��� ��������� ������ ������
	TMatrixDimension tidMu = 0;
	TMatrixDimension tidCurrentElement = 0;
	// ���������� ��������������� ��������
	TMatrixDimension tid = 0;
	// �������� ���������, ����������� ��� ��������� ������ ������
	TMatrix currentElement = 0;
	TMatrix mu = 0;
	// ������������ �������� tid
	TMatrixDimension maxTid = 0;

	// �������� ��������� ���������� (���������� �� ����������)
	row0 = *dev_row0;
	column0 = *dev_column0;
	n = *dev_n;
	m = *dev_m;

	// ������� �� ������������ �����-������ � �������� ����������� ����������
	tidLocal = threadIdx.x + blockIdx.x * blockDim.x;
	// ���������� �������� ����������
	GetSizeSubMatrixGPU(n, m, row0, column0, nLocal, mLocal);
	
	// �������� �������� (�������� ��������� tidLocal)
	GetMaxTidLocalGPU(nLocal, mLocal, maxTid);

	// ��������� ��� ��������, ������� ������������� ������� ������
	while (tidLocal < maxTid)
	{
		// �� �� ������������� ������� ������� ���������, ��� ��� ��� ���������� ��� ��������� ������ ������

		// �������� �������� ���������� ������� ���������� ������
		TransformatioCoordsTidGPU(tidLocal, tid, mLocal, m, row0, column0);
		// �������� �������� ���������� ������� mu
		GetMuTidGPU(tidLocal, mLocal, row0, column0, m, tidMu);
		GetCurrentElementTidGPU(tidLocal, mLocal, row0, column0, m, tidCurrentElement);

		// ���������� �������� ���������, ����������� ��� ��������� ������ ������
		mu = a[tidMu];
		currentElement = a[tidCurrentElement];
		a[tid] = a[tid] - mu * currentElement;

		// �������� ��������� ������� ��� �����������
		tidLocal += gridDim.x * blockDim.x;
	}
	return;
}

/*
 * ���������� ������������� ��� ������ ������
 */
__global__ void GetMuGPU(TMatrix *a, TMatrixDimension *dev_column0, TMatrixDimension *dev_row0, TMatrixDimension *dev_m)
{
	TMatrixDimension tid = 0;
	TMatrix mu = 0;
	TMatrixDimension maxTid = 0;
	TMatrixDimension tidGeneral = 0;
	TMatrixDimension column0 = 0;
	TMatrixDimension row0 = 0;
	TMatrixDimension m = 0;
	TMatrix element = 0;
	TMatrix generalElement = 0;
	TMatrixDimension tidLocal = 0;

	column0 = *dev_column0;
	row0 = *dev_row0;
	m = *dev_m;

	maxTid = (row0 + 1) * m;
	// ����� ������
	tidLocal = threadIdx.x + blockIdx.x * blockDim.x;
	// ������� ������� (���, �� ������� �����, �.�. ����� ������� � ������ LUP-����������)
	tidGeneral = GetLineIndexGPU(row0, column0, m);
	tid = tidGeneral + tidLocal;
	generalElement = a[tidGeneral];
	while (tid < maxTid)
	{
		element = a[tid];
		mu = element / generalElement;
		a[tid] = mu;
		// ������� � ���������� (�������������) �����
		tid += blockDim.x * gridDim.x;
	}
	return;
}

/*
 * �������� ������������ �� ������ ������� ��� ���������� LUP ����������.
 */
__global__ void GetIndexOfMaxElementGPU(TMatrix *a, TMatrixDimension *column, TMatrixDimension *row, TMatrixDimension *n, TMatrixDimension *m, TMatrixDimension *indexOfMaxElement)
{
	TMatrix maxElement = 0;
	TMatrixDimension lineIndex = 0;

	*indexOfMaxElement = -1;
	for (; *column < *m; ++(*column) )
	{
		for (TMatrixDimension j = *row; j < *n; ++j)
		{
			lineIndex = GetLineIndexGPU(j, *column, *m);
			if (*indexOfMaxElement == -1 && abs(a[lineIndex]) > eps || abs(maxElement) < abs(a[lineIndex]) && abs(a[lineIndex]) > eps)
			{
				maxElement = a[lineIndex];
				*indexOfMaxElement = lineIndex;
			}
		}
		if (*indexOfMaxElement != -1)
		{
			break;
		}
	}
	return;
}


/*
 * ����� ����� �� GPU
 */
__global__ void SwapGPU(TMatrix *a, TMatrixDimension *dev_m, TMatrixDimension *dev_row1, TMatrixDimension *dev_row2)
{
	
	TMatrixDimension tid = threadIdx.x + blockIdx.x * blockDim.x;
	TMatrixDimension tidMax;
	TMatrixDimension tid1;
	TMatrixDimension tid2;
	TMatrixDimension m = *dev_m;
	TMatrixDimension row2 = *dev_row2;
	TMatrixDimension row1 = *dev_row1;

	tidMax = m;
	while (tid < tidMax)
	{
		tid1 = m * row1 + tid;
		tid2 = m * row2 + tid;
		SwapElementsGPU(a[tid1], a[tid2]);
		tid += gridDim.x * blockDim.x;
	}
	return;
}



/*___________________CPU_GENERAL_FUNCTIONS___________________*/





/*
 * �������� ������� ������� � ������ ������ � ������� ��������� ������� � i-��
*/
void GetGeneralElement(TMatrix *dev_a, 
					   TMatrixDimension n, TMatrixDimension m, 
					   TMatrixDimension *curColumn, TMatrixDimension curRow)
{
	TMatrixDimension threads = 0;
	TMatrixDimension blocks = 0;
	TMatrixDimension curRowToChange = 0;
	TMatrixDimension lineIndex = 0;

	TMatrixDimension *dev_curRow = NULL;
	TMatrixDimension *dev_curColumn = NULL;
	TMatrixDimension *dev_n = NULL;
	TMatrixDimension *dev_m = NULL;
	TMatrixDimension *dev_lineIndex = NULL;
	TMatrixDimension *dev_curRowToChange = NULL;

	// ������������� ������ �� GPU
	HANDLE_ERROR(cudaMalloc(&dev_curRow, sizeof(TMatrixDimension) ) );
	HANDLE_ERROR(cudaMalloc(&dev_curColumn, sizeof(TMatrixDimension) ) );
	HANDLE_ERROR(cudaMalloc(&dev_n, sizeof(TMatrixDimension) ) );
	HANDLE_ERROR(cudaMalloc(&dev_m, sizeof(TMatrixDimension) ) );
	HANDLE_ERROR(cudaMalloc(&dev_lineIndex, sizeof(TMatrixDimension) ) );
	HANDLE_ERROR(cudaMalloc(&dev_curRowToChange, sizeof(TMatrixDimension) ) );
	
	// ������� ������ �� GPU
	HANDLE_ERROR(cudaMemcpy(dev_curRow, &curRow, sizeof(TMatrixDimension), cudaMemcpyHostToDevice) );
	HANDLE_ERROR(cudaMemcpy(dev_curColumn, curColumn, sizeof(TMatrixDimension), cudaMemcpyHostToDevice) );
	HANDLE_ERROR(cudaMemcpy(dev_n, &n, sizeof(TMatrixDimension), cudaMemcpyHostToDevice) );
	HANDLE_ERROR(cudaMemcpy(dev_m, &m, sizeof(TMatrixDimension), cudaMemcpyHostToDevice) );

	
	// ����� ������������� �� ������ ���������� ��������
	// ��� ����, ����� �� ������������� �������� � �� �������� � �����������, ���������� ���� �����, ������ ���� ������������ ������� ��� ������ ������
	GetIndexOfMaxElementGPU<<<1, 1>>>(dev_a, dev_curColumn, dev_curRow, dev_n, dev_m, dev_lineIndex);
	HANDLE_ERROR(cudaGetLastError() );
	HANDLE_ERROR(cudaMemcpy(curColumn, dev_curColumn, sizeof(TMatrixDimension), cudaMemcpyDeviceToHost) );
	HANDLE_ERROR(cudaMemcpy(&lineIndex, dev_lineIndex, sizeof(TMatrixDimension), cudaMemcpyDeviceToHost) );

	if (lineIndex != -1)
	{
		// ��������� ����� ������, ����������� ������������� ��������, �� ��������� �������
		curRowToChange = GetRowByLineIndexCPU(lineIndex, m);

		HANDLE_ERROR(cudaMemcpy(dev_curRowToChange, &curRowToChange, sizeof(TMatrixDimension), cudaMemcpyHostToDevice) );
		// �������� ������ ����� ������ � ������� �� ��������� ����� ����������� ���������
		GetBlockThread((n - curRow + 1) * (m - *curColumn + 1), blocks, threads);
		// ����� ������� �����
		SwapGPU<<<blocks, threads>>>(dev_a, dev_m, dev_curRow, dev_curRowToChange);
		HANDLE_ERROR(cudaGetLastError() );
	}


	// ������� ������ �� GPU
	HANDLE_ERROR(cudaFree(dev_curRow) );
	HANDLE_ERROR(cudaFree(dev_curColumn) );
	HANDLE_ERROR(cudaFree(dev_n) );
	HANDLE_ERROR(cudaFree(dev_m) );
	HANDLE_ERROR(cudaFree(dev_lineIndex) );
	HANDLE_ERROR(cudaFree(dev_curRowToChange) );
	return;
}


/*
 * ���������� ����� (�������� �������)
 */

TMatrixDimension GetRangGPU(TMatrix *dev_a, TMatrixDimension n, TMatrixDimension m)
{
	TMatrixDimension column = 0;
	TMatrixDimension row = 0;
	TMatrixDimension columnTmp = 0;
	TMatrixDimension rowTmp = 0;
	TMatrixDimension blocks = 0;
	TMatrixDimension threads = 0;

	// ������� ������� � ������ ������� a_ij ��������������
	TMatrixDimension *dev_row;
	TMatrixDimension *dev_column;
	TMatrixDimension *dev_n;
	TMatrixDimension *dev_m;

	// ��������� ������ �� GPU
	HANDLE_ERROR(cudaMalloc(&dev_row, sizeof(TMatrixDimension) ) );
	HANDLE_ERROR(cudaMalloc(&dev_column, sizeof(TMatrixDimension) ) );
	HANDLE_ERROR(cudaMalloc(&dev_n, sizeof(TMatrixDimension) ) );
	HANDLE_ERROR(cudaMalloc(&dev_m, sizeof(TMatrixDimension) ) );
	
	HANDLE_ERROR(cudaMemcpy(dev_m, &m, sizeof(TMatrixDimension), cudaMemcpyHostToDevice) );
	HANDLE_ERROR(cudaMemcpy(dev_n, &n, sizeof(TMatrixDimension), cudaMemcpyHostToDevice) );

	for (row = 0; row < n; ++row)
	{
		// �������� ������� ������� � ������ ������ � ������� ��������� ������� � i-��
		GetGeneralElement(dev_a, n, m, &column, row);
		if (column >= m)
		{
			break;
		}

		// �������� �������� �� GPU
		HANDLE_ERROR(cudaMemcpy(dev_column, &column, sizeof(TMatrixDimension), cudaMemcpyHostToDevice) );
		HANDLE_ERROR(cudaMemcpy(dev_row, &row, sizeof(TMatrixDimension), cudaMemcpyHostToDevice) );

		// �������� ������ ����� ������ � ������� �� ��������� ����� ����������� ���������
		GetBlockThread((n - row + 1) * (m - column + 1), blocks, threads);
		// ����������� ���������� �� ������� L
		GetMuGPU<<<blocks, threads>>>(dev_a, dev_column, dev_row, dev_m);
		HANDLE_ERROR(cudaGetLastError() );



		// �� ����������� ������ ������� � ������ ������
		columnTmp = column + 1;
		rowTmp = row + 1;
		// �������� �������� �� GPU
		HANDLE_ERROR(cudaMemcpy(dev_column, &columnTmp, sizeof(TMatrixDimension), cudaMemcpyHostToDevice) );
		HANDLE_ERROR(cudaMemcpy(dev_row, &rowTmp, sizeof(TMatrixDimension), cudaMemcpyHostToDevice) );
		// �������� ������ ����� ������ � ������� �� ��������� ����� ����������� ���������
		GetBlockThread((n - row) * (m - column), blocks, threads);
		// ����������� �������
		ModifyMatrixGPU<<<blocks, threads>>>(dev_a, dev_column, dev_row, dev_n, dev_m);
		HANDLE_ERROR(cudaGetLastError() );
		++column;
	}
	
	cudaFree(dev_row);
	cudaFree(dev_column);
	cudaFree(dev_n);
	cudaFree(dev_m);
	return row;
}

int main(int argc, char* argv[] ) 
{
	/*___________________CPU_DEFINITION___________________*/
	// ���������� ��� �������� dev_ ������������ �� CPU
	// ���������� � ��������� dev_ ������������ �� GPU


	// a -- �������, ���� ������� ������� ���������
    TMatrix *a;

	// n, m -- ����������� ������� (a_nm)
	TMatrixDimension n, m;

	// rang -- ���� ������� (a_nm), ���������� � CPU � GPU �������������
	TMatrixDimension rangGPU;
	// TMatrixDimension rangCPU;




	/*___________________GPU_DEFINITION___________________*/
	// dev_a -- �������, ���� ������� ������� ���������
    TMatrix *dev_a;


	rangGPU = 0;
	// rangCPU = 0;
	/*___________________GENERAL_SOURCE___________________*/
	cin >> n >> m;  

	a = ReadElemetsOfMatrixCPU(n, m);

	
	HANDLE_ERROR(cudaMalloc(&dev_a, sizeof(TMatrix) * n * m) );
	HANDLE_ERROR(cudaMemcpy(dev_a, a, sizeof(TMatrix) * n * m, cudaMemcpyHostToDevice) );
	
	rangGPU = GetRangGPU(dev_a, n, m);
    // rangCPU = GetRangCPU(a, n, m);


	// �������� �������� ������������ (��� ������������)

    free(a);
	HANDLE_ERROR(cudaFree(dev_a ) );

	cout << rangGPU << endl;

    return 0;
}


/*___________________OTHER_FUNCTIONS___________________*/

/*
 * ������ �������
 */
TMatrix * ReadElemetsOfMatrixCPU(TMatrixDimension n, TMatrixDimension m)
{
	// ��������� ������ �� CPU
     TMatrix *a = (TMatrix *) malloc(n * m * sizeof(TMatrix) );
	TMatrix element = 0;

	for (TMatrixDimension i = 0; i < n; ++i)
	{
		for (TMatrixDimension j = 0; j < m; ++j)
		{
			/*
			 * �������� ������ �������
			 */
			TMatrixDimension index;
			index = GetLineIndexCPU(i, j, m);
			cin >> element;
			a[index] = element;
		}
	}
	return a;
}

/*
 * ���������� ��������� ������� �������
 */
TMatrixDimension GetLineIndexCPU(TMatrixDimension i, TMatrixDimension j, TMatrixDimension m)
	/*
	 * i -- ����� ������
	 * j -- ����� �������
	 * m -- ����� ��������
	 */
{
	return j + i * m;
}

/*
 * ���������� ��������� ������� �������
 */
__device__ TMatrixDimension GetLineIndexGPU(TMatrixDimension i, TMatrixDimension j, TMatrixDimension m)
	/*
	 * i -- ����� ������
	 * j -- ����� �������
	 * m -- ����� ��������
	 */
{
	return j + i * m;
}

/*
 * ���������� ������ �� ��������� �������
 */
TMatrixDimension GetRowByLineIndexCPU(TMatrixDimension lineIndex, TMatrixDimension m)
	/*
	 * lineIndex -- �������� ������ �������
	 * m -- ����� ��������
	 */
{
	return lineIndex / m;
}

/*
 * ���������� ������� �� ��������� �������
 */
TMatrixDimension GetColumnByLineIndexCPU(TMatrixDimension lineIndex, TMatrixDimension m)
	/*
	 * lineIndex -- �������� ������ �������
	 * m -- ����� ��������
	 */
{
	return lineIndex % m;
}

/* 
 * ��������� ����� ������ � ������� ��� ��������� ���������� �������������� �����
 */
void GetBlockThread(TMatrixDimension poles, TMatrixDimension &blocks, TMatrixDimension &threads)
{
	// ������ ����� ������� 30 �����. ����� ������� � ����� ������ ���� �� ����� 32 * 32. ������������ ���������� ������ 64 * 64
	TMatrixDimension polesPerThreads = 30;
	TMatrixDimension allThreads = 0;
	TMatrixDimension maxThreads = 16 * 16;
	if (poles < polesPerThreads)
	{
		blocks = 1;
		threads = 1;
		return;
	}
	allThreads = poles / polesPerThreads + (poles % polesPerThreads == 1);
	blocks = allThreads / maxThreads + allThreads % maxThreads;
	threads = maxThreads;

	return;
}

__device__ void SwapElementsGPU(TMatrix &a, TMatrix &b)
{
	TMatrix tmp = 0;
	tmp = a;
	a = b;
	b = tmp;
}

__device__ void GetMaxTidLocalGPU(TMatrixDimension nLocal, TMatrixDimension mLocal, TMatrixDimension &tidMax)
{
	tidMax = nLocal * mLocal;
}

__device__ void GetSizeSubMatrixGPU(TMatrixDimension n, TMatrixDimension m,
									 TMatrixDimension row, TMatrixDimension column, 
									 TMatrixDimension &nLocal, TMatrixDimension &mLocal)
{
	nLocal = n - row;
	mLocal = m - column;
}

__device__ void TransformatioCoordsTidGPU(TMatrixDimension tidLocal, TMatrixDimension &tid,
										TMatrixDimension mLocal, TMatrixDimension m,
										TMatrixDimension row0, TMatrixDimension column0)
{
	TMatrixDimension sum1 =  0;
	TMatrixDimension sum2 =  0;
	sum1 = (tidLocal / mLocal + 1) * column0;
	sum2 = row0 * m;
	tid = sum1 + sum2 + tidLocal;
}

__device__ void GetMuTidGPU(TMatrixDimension tidLocal, 
						  TMatrixDimension mLocal, 
						  TMatrixDimension row0, TMatrixDimension column0,
						  TMatrixDimension m,
						  TMatrixDimension &tidLocalMu)
{
	TMatrixDimension rowMu = 0;
	TMatrixDimension columnMu = 0;

	rowMu = tidLocal / mLocal + row0;
	columnMu = column0 - 1;

	tidLocalMu = rowMu * m + columnMu;
}

__device__ void GetCurrentElementTidGPU(TMatrixDimension tidLocal, 
						  TMatrixDimension mLocal, 
						  TMatrixDimension row0, TMatrixDimension column0,
						  TMatrixDimension m,
						  TMatrixDimension &tidLocalCurrentElement)
{
	TMatrixDimension rowCurrentElement = 0;
	TMatrixDimension columnCurrentElement = 0;

	columnCurrentElement = tidLocal % mLocal + column0;
	rowCurrentElement = row0 - 1;

	tidLocalCurrentElement = rowCurrentElement * m + columnCurrentElement;
}