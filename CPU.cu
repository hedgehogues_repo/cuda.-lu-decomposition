#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "CPU.h"

using namespace std;

/*___________________CPU_FUNCTIONS___________________*/

/*
 * �������� �������� ������ � �������
 */
long int GetLineIndexCPU(TMatrixDimension i, TMatrixDimension j, TMatrixDimension m)
{
	return j + i * m;
}

/*
 * �������� ������ �������
 */
void PrintMatrix(TMatrix *a, TMatrixDimension n, TMatrixDimension m)
{
	TMatrixDimension lineIndex = 0;
	cout << endl;
	for (TMatrixDimension i = 0; i < n; ++i)
	{
		for (TMatrixDimension j = 0; j < m; ++j)
		{
			lineIndex = GetLineIndexCPU(i, j, m);
			cout << a[lineIndex] << " ";
		}
		cout << endl;
	}
	cout << endl;
	return;
}

/*
 * ���������� ������ �� ��������� �������
 */
TMatrixDimension GetRowByLineIndex(TMatrixDimension lineIndex, TMatrixDimension m)
	/*
	 * lineIndex -- �������� ������ �������
	 * m -- ����� ��������
	 */
{
	return lineIndex / m;
}

/*
 * ���������� ������� �� ��������� �������
 */
TMatrixDimension GetColumnByLineIndex(TMatrixDimension lineIndex, TMatrixDimension m)
	/*
	 * lineIndex -- �������� ������ �������
	 * m -- ����� ��������
	 */
{
	return lineIndex % m;
}

/*
 * ���������� ij-������� ������������� �������� column-�� ������� �������
 */
TMatrixDimension GetIndexOfMaxElementCPU(TMatrix *a, TMatrixDimension *column, TMatrixDimension row, TMatrixDimension n, TMatrixDimension m)
{
	TMatrixDimension size = n;
	TMatrix maxElement = 0;
	TMatrixDimension indexOfMaxElement = -1;
	TMatrixDimension lineIndex = 0;
	for (; *column < m; ++(*column) )
	{
		for (TMatrixDimension j = row; j < n; ++j)
		{
			lineIndex = GetLineIndexCPU(j, *column, m);
			if (indexOfMaxElement == -1 && abs(a[lineIndex]) > eps || abs(maxElement) < abs(a[lineIndex]) && abs(a[lineIndex]) > eps)
			{
				maxElement = a[lineIndex];
				indexOfMaxElement = lineIndex;
			}
		}
		if (indexOfMaxElement != -1)
		{
			break;
		}
	}
	return indexOfMaxElement;
}

/*
 * ������������ ����� �����
 */
void SwapCPU(TMatrix *a, TMatrixDimension m, TMatrixDimension row1, TMatrixDimension row2)
{
	TMatrixDimension i;
	TMatrixDimension lineIndex1;
	TMatrixDimension lineIndex2;

	for (i = 0; i < m; ++i)
	{
		// ��������� �������� ��������
		lineIndex1 = GetLineIndexCPU(row1, i, m);
		lineIndex2 = GetLineIndexCPU(row2, i, m);
		swap(a[lineIndex1], a[lineIndex2]);
	}
	return;
}

/*
 * ��������� ������������� �������� � ����� �����
 */
void GetMaxElementsAndSwapRowsCPU(TMatrix *a, TMatrixDimension n, TMatrixDimension m, TMatrixDimension *curColumn, TMatrixDimension curRow)
{
	// �������� ������ ������������� ��������
	TMatrixDimension lineIndex = 0;
	// ����� ������, ������� ������� ��������
	TMatrixDimension curRowToChange = 0;
	// �������� ������������ ������� � �������, ������� � curRow-�� ������
	lineIndex = GetIndexOfMaxElementCPU(a, curColumn, curRow, n, m);
	if (lineIndex != -1)
	{
		// �������� ����� ������, ������� ������� ��������
		curRowToChange = GetRowByLineIndex(lineIndex, m);
		// ������ 2 ������ ������� (curRow � curRowToChange)
		SwapCPU(a, m, curRow, curRowToChange);
	}
	return;
}

/*
 * ����������� ������� ������� �� CPU
 */
void ModifyMatrixCPU(TMatrix *a, TMatrixDimension column, TMatrixDimension row, TMatrixDimension n, TMatrixDimension m)
{
	// �������� ��� ������ ������ �������
	TMatrixDimension i = 0;
	TMatrixDimension j = 0;

	// �������� ������ ��������������� ij-��������
	TMatrixDimension lineIndex = 0;
	// �������� ������ ������������� �������� i ������
	TMatrixDimension lineIndexCoeff = 0;
	TMatrixDimension lineIndexGeneralElement= 0;
	TMatrixDimension lineIndexGeneralCoeff= 0;
	TMatrix mu = 0;
	TMatrix currentElement = 0;


	for (i = row + 1; i < n; ++i)
	// �������� �� �������
	{
		lineIndexGeneralCoeff = GetLineIndexCPU(row, column, m);
		lineIndexGeneralElement = GetLineIndexCPU(i, column, m);
		currentElement = a[lineIndexGeneralElement];
		for (j = column + 1; j < m; ++j)
		// �������� �� ��������
		{
			lineIndex = GetLineIndexCPU(i, j, m);
			lineIndexCoeff = GetLineIndexCPU(row, j, m);
			mu = a[lineIndexCoeff] / a[lineIndexGeneralCoeff];
			a[lineIndex] = a[lineIndex] - mu * currentElement;
		}
	}
	return;
}

/* 
 * ���������� ����� ������� �� CPU
 */
TMatrixDimension GetRangCPU(TMatrix *a, TMatrixDimension n, TMatrixDimension m)
{
	TMatrixDimension j = 0;
	TMatrixDimension i = 0;
	TMatrixDimension rang = 0;
	for (i = 0; i < n; ++i)
	{
		// �������� ������� ������� � ������ ������ � ������� ��������� ������� � i-��
		GetMaxElementsAndSwapRowsCPU(a, n, m, &j, i);
		PrintMatrix(a, n, m);
		if (j >= m)
		{
			// ���� ������� �� ������� ������� ��-�� �������� ���������� ������� ��� ��� �����, �� �������
			return i;
		}
		// ������������ ������� a, ���� ������������ ������������ ��������������
		ModifyMatrixCPU(a, j, i, n, m);
		PrintMatrix(a, n, m);
		++j;
	}
	return i;
}