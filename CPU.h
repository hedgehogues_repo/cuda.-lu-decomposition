#define eps 0.00000000001
typedef double TMatrix;
typedef int   TMatrixDimension;

TMatrixDimension GetRangCPU(TMatrix *a, TMatrixDimension n, TMatrixDimension m);
void PrintMatrix(TMatrix *a, TMatrixDimension n, TMatrixDimension m);